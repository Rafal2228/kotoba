(function(){
    var app = angular.module('kotobaControllers', ['ui.router']);

    app.controller('navBarController', ['$scope', '$state', function($scope, $state){

        $scope.$on('$stateChangeSuccess', function(){
            console.log($state.current.name);
            $scope.currentState = $state.current.name;
        });
    }]);

    app.controller('homeController', [function(){

    }]);

    app.controller('addWordController', [function(){

    }]);

    app.controller('removeWordController', [function(){

    }]);
})();