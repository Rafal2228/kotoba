(function(){
    var app = angular.module('kotobaMain', ['ui.router', 'kotobaDirectives', 'kotobaControllers']);

    app.config(function($stateProvider, $locationProvider, $urlRouterProvider){
        $locationProvider.html5Mode(true);

        $stateProvider
            .state('home',{
            url: "/home",
            templateUrl: "views/home.html",
            controller: "homeController",
            controllerAs: "homeCtrl"
        }).state('addWord',{
            url: "/addWord",
            templateUrl: "views/addWord.html",
            controller: "addWordController",
            controllerAs: "addCtrl"
        }).state('removeWord',{
            url: "/removeWord",
            templateUrl: "views/removeWord.html",
            controller: "removeWordController",
            controllerAs: "removeCtrl"
        });

    $urlRouterProvider.when('/', '/home');
    });

})();